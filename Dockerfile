FROM arm32v6/alpine:latest
MAINTAINER Alex Jones <alex.jones@unclealex.co.uk>

WORKDIR /opt/docker
COPY . /opt/docker
COPY ./cmd.sh /usr/bin
RUN apk update && \
    apk add --no-cache python socat libusb && \
    python -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip install --upgrade pip setuptools && \
    pip install pyusb && \
    rm -r /root/.cache && \
    pip install snmp-passpersist && \
    python setup.py install && \
    rm -rf ./*

ENTRYPOINT ["cmd.sh"]
